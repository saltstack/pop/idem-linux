import io
import pytest
import mock

NQN_DATA = """
nqn.testnqn:234236
nqn.testnqn.test:23958
"""


@pytest.mark.asyncio
async def test_load_nvme_nqn(mock_hub, hub):
    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open", return_value=io.StringIO(NQN_DATA)
        ):
            mock_hub.grains.linux.hw.storage.nvme.load_nvme_nqn = (
                hub.grains.linux.hw.storage.nvme.load_nvme_nqn
            )
            await mock_hub.grains.linux.hw.storage.nvme.load_nvme_nqn()

    assert mock_hub.grains.GRAINS.nvme_nqn == (
        "nqn.testnqn:234236",
        "nqn.testnqn.test:23958",
    )
