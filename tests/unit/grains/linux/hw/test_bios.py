import io
import pytest
import mock
from dict_tools import data


@pytest.mark.asyncio
async def test_load_arm_linux(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"retcode": 0, "stdout": "manufacturer=testman"}),
        data.NamespaceDict({"retcode": 0, "stdout": "DeviceDesc=testdesc"}),
        data.NamespaceDict({"retcode": 0, "stdout": "serial#=testnum"}),
    ]

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.linux.hw.bios.load_arm_linux = (
            hub.grains.linux.hw.bios.load_arm_linux
        )
        await mock_hub.grains.linux.hw.bios.load_arm_linux()

    assert mock_hub.grains.GRAINS.manufacturer == "testman"
    assert mock_hub.grains.GRAINS.serialnumber == "testnum"
    assert mock_hub.grains.GRAINS.productname == "testdesc"


@pytest.mark.asyncio
async def test_load_dmi(mock_hub, hub):
    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open",
            side_effect=[
                io.StringIO("01/01/0001"),
                io.StringIO("testversion"),
                io.StringIO("testman"),
                io.StringIO("testprod"),
                io.StringIO("testserial"),
                io.StringIO("00000000-0000-0000-0000-000000000000"),
            ],
        ):
            mock_hub.grains.linux.hw.bios.load_dmi = hub.grains.linux.hw.bios.load_dmi
            await mock_hub.grains.linux.hw.bios.load_dmi()

    assert mock_hub.grains.GRAINS.biosreleasedate == "01/01/0001"
    assert mock_hub.grains.GRAINS.biosversion == "testversion"
    assert mock_hub.grains.GRAINS.manufacturer == "testman"
    assert mock_hub.grains.GRAINS.productname == "testprod"
    assert mock_hub.grains.GRAINS.serialnumber == "testserial"
    assert mock_hub.grains.GRAINS.uuid == "00000000-0000-0000-0000-000000000000"


@pytest.mark.asyncio
async def test_load_smbios(mock_hub, hub):
    mock_hub.exec.linux.smbios.get.side_effect = [
        "01/01/0001",
        "testversion",
        "testman",
        "testprod",
        "testserial",
        "00000000-0000-0000-0000-000000000000",
    ]
    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.linux.hw.bios.load_smbios = hub.grains.linux.hw.bios.load_smbios
        await mock_hub.grains.linux.hw.bios.load_smbios()

    assert mock_hub.grains.GRAINS.biosreleasedate == "01/01/0001"
    assert mock_hub.grains.GRAINS.biosversion == "testversion"
    assert mock_hub.grains.GRAINS.manufacturer == "testman"
    assert mock_hub.grains.GRAINS.productname == "testprod"
    assert mock_hub.grains.GRAINS.serialnumber == "testserial"
    assert mock_hub.grains.GRAINS.uuid == "00000000-0000-0000-0000-000000000000"


@pytest.mark.asyncio
async def test_load_serialnumber(mock_hub, hub):
    mock_hub.exec.linux.smbios.get.return_value = "testserial"

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.linux.hw.bios.load_serialnumber = (
            hub.grains.linux.hw.bios.load_serialnumber
        )
        await mock_hub.grains.linux.hw.bios.load_serialnumber()

    assert mock_hub.grains.GRAINS.serialnumber == "testserial"
