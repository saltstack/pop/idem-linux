import pytest


@pytest.mark.asyncio
async def test_load_lsb_release(mock_hub, hub):
    # TODO LSB_RELEASE needs a refactor, test it more thoroughly when it's refactored
    mock_hub.grains.linux.os.lsb_release.load_lsb_release = (
        hub.grains.linux.os.lsb_release.load_lsb_release
    )
    await mock_hub.grains.linux.os.lsb_release.load_lsb_release()
